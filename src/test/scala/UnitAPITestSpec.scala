import cats.effect._
import doobie.util.ExecutionContexts
import expression.Calculator
import io.circe.Json
import io.circe.literal._
import org.http4s._
import org.http4s.circe._
import service.UnitConversionService
import org.http4s.dsl.io._
import org.scalatest._
import org.http4s._
import org.http4s.implicits._

class UnitAPITestSpec extends FunSpec {
  implicit val cs = IO.contextShift(ExecutionContexts.synchronous)
  val service     = new UnitConversionService[IO]().service()

  val hectareJson = json"""{
    "unit_name" : "m2",
    "multiplication_factor" : 1E+4
  }
"""

  val degreeJson = json"""{
"unit_name" : "rad",
"multiplication_factor" : 0.01745329251994
}
"""

  val degreeTimesHectareJson = json"""
                                     {
                                      "unit_name" : "rad*m2",
                                       "multiplication_factor" : 174.53292519943295
                                     }
                                     """

  val degreeTimesHectareWithParentheses = json"""{
    "unit_name" : "(rad*m2)",
    "multiplication_factor" : 174.53292519943295
  }
"""

  describe("when you pass in a string for calculator to parse it does so correctly") {
    describe("for provided BigDecimal ") {
      val string1        = (Math.PI / 180).toString
      val expectedResult = 0.017453292519943295
      it("should have 0 errors") {
        val result = (Calculator.evaluate(string1))
        assert(expectedResult == result)
      }
    }
  }

  describe("when a bad URL path is called") {
    describe("for invalid request params ") {
      it("should return Not Found") {
        val request  = Request[IO](GET, uri"/units/si/units=hectacre")
        val response = service.orNotFound.run(request).unsafeRunSync()
        assert(response.status == NotFound)
      }
    }
  }

  describe("when unsupported units are provided for request") {
    describe("for valid request ") {
      it("should return 200 but have a Bad Input for unit_name") {
        val request  = Request[IO](GET, uri"/units/si?units=hectacre")
        val response = service.orNotFound.run(request).unsafeRunSync()
        assert(response.status == Ok)
        assert(response.as[Json].unsafeRunSync().toString().contains("Bad Input"))
      }
    }
  }

  describe("when supported hectare units are provided for request") {
    describe("for valid request ") {
      it("should return 200 and correctly converted unit") {
        val request  = Request[IO](GET, uri"/units/si?units=hectare")
        val response = service.orNotFound.run(request).unsafeRunSync()
        assert(response.status == Ok)
        assert(response.as[Json].unsafeRunSync() == hectareJson)
      }
    }
  }

  describe("when supported degree units are provided for request") {
    describe("for valid request ") {
      it("should return 200 and correctly converted unit") {
        val request  = Request[IO](GET, uri"/units/si?units=degree")
        val response = service.orNotFound.run(request).unsafeRunSync()
        assert(response.status == Ok)
        assert(response.as[Json].unsafeRunSync() == degreeJson)
      }
    }
  }

  describe("when supported degree times hectare units are provided for request") {
    describe("for valid request ") {
      it("should return 200 and correctly converted unit") {
        val request  = Request[IO](GET, uri"/units/si?units=degree*hectare")
        val response = service.orNotFound.run(request).unsafeRunSync()
        assert(response.status == Ok)
        assert(response.as[Json].unsafeRunSync() == degreeTimesHectareJson)
      }
    }
  }

  describe("when supported degree times hectare units with parentheses that are balanced are provided for request") {
    describe("for valid request ") {
      it("should return 200 and correctly converted unit") {
        val request  = Request[IO](GET, uri"/units/si?units=(degree*hectare)")
        val response = service.orNotFound.run(request).unsafeRunSync()
        assert(response.status == Ok)
        assert(response.as[Json].unsafeRunSync() == degreeTimesHectareWithParentheses)
      }
    }
  }

  describe("when supported degree times hectare units with imbalanced parentheses are provided for request") {
    describe("for valid request ") {
      it("should return 400") {
        val request  = Request[IO](GET, uri"/units/si?units=(degree*hectare")
        val response = service.orNotFound.run(request).unsafeRunSync()
        assert(response.status == BadRequest)
      }
    }
  }

}
