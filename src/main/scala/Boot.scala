import cats.effect._
import cats.implicits._
import config._
import service.UnitConversionService
import org.http4s.server.blaze._
import org.http4s.implicits._
import org.http4s.server.Router

object Boot extends IOApp {

  def createServer[F[_]: ContextShift: ConcurrentEffect: Timer]: Resource[F, ExitCode] =
    for {
      config                <- Resource.liftF(Config.load[F]())
      unitConversionService = new UnitConversionService().service()
      httpApp               = Router("/" -> unitConversionService).orNotFound

      exitCode <- Resource.liftF(
                   BlazeServerBuilder[F]
                     .bindHttp(config.server.port, config.server.host)
                     .withHttpApp(httpApp)
                     .serve
                     .compile
                     .drain
                     .as(ExitCode.Success)
                 )

    } yield exitCode

  override def run(args: List[String]): IO[ExitCode] =
    createServer.use(IO.pure) // Use IO as the effect type
}
