package model

import cats.implicits._

final case class SiUnitModel(
    name: String,
    symbol: String,
    multiplicationFactor: BigDecimal,
    unit: String
)

object SiUnitModel {
  def lookup(unit: String): Option[(String, BigDecimal)] =
    unit match {
      case minute.name | minute.symbol       => (minute.unit, minute.multiplicationFactor).some
      case hour.name | hour.symbol           => (hour.unit, hour.multiplicationFactor).some
      case day.name | day.symbol             => (day.unit, day.multiplicationFactor).some
      case degree.name | degree.symbol       => (degree.unit, degree.multiplicationFactor).some
      case arcminute.name | arcminute.symbol => (arcminute.unit, arcminute.multiplicationFactor).some
      case arcsecond.name | arcsecond.symbol => (arcsecond.unit, arcsecond.multiplicationFactor).some
      case hectare.name | hectare.symbol     => (hectare.unit, hectare.multiplicationFactor).some
      case litre.name | litre.symbol         => (litre.unit, litre.multiplicationFactor).some
      case tonne.name | tonne.symbol         => (tonne.unit, tonne.multiplicationFactor).some
      case _                                 => none
    }

  val minute = SiUnitModel("minute", "min", 60, "s")
  val hour   = SiUnitModel("hour", "h", 3600, "s")
  val day    = SiUnitModel("day", "d", 86400, "s")

  val degree    = SiUnitModel("degree", "°", Math.PI / 180, "rad")
  val arcminute = SiUnitModel("arcminute", "'", Math.PI / 10800, "rad")
  val arcsecond = SiUnitModel("arcsecond", "\"", Math.PI / 648000, "rad")

  val hectare = SiUnitModel("hectare", "ha", 10000, "m2")
  val litre   = SiUnitModel("litre", "L", 0.001, "m3")
  val tonne   = SiUnitModel("tonne", "t", 1000, "kg")
}
