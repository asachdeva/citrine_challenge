package expression

import scala.util.parsing.combinator._

sealed trait Expression
case class Number(value: BigDecimal)                                       extends Expression
case class UnaryOp(operator: String, expression: Expression)               extends Expression
case class BinaryOp(operator: String, left: Expression, right: Expression) extends Expression

object ExpressionParser extends JavaTokenParsers with PackratParsers {

  lazy val expr: PackratParser[Expression] =
    (expr <~ "+") ~ term ^^ { case left ~ right   => BinaryOp("+", left, right) } |
      (expr <~ "-") ~ term ^^ { case left ~ right => BinaryOp("-", left, right) } |
      term

  lazy val term: PackratParser[Expression] =
    (term <~ "*") ~ factor ^^ { case left ~ right   => BinaryOp("*", left, right) } |
      (term <~ "/") ~ factor ^^ { case left ~ right => BinaryOp("/", left, right) } |
      factor

  lazy val factor: PackratParser[Expression] =
    (factor <~ "^") ~ exp ^^ { case left ~ right => BinaryOp("^", left, right) } |
      exp

  lazy val exp: PackratParser[Expression] =
    (expr ~ "!") ^^ { case left ~ _    => UnaryOp("!", left) } |
      ("+" ~ expr) ^^ { case _ ~ right => UnaryOp("+", right) } |
      ("-" ~ expr) ^^ { case _ ~ right => UnaryOp("-", right) } |
      ("|" ~> expr <~ "|") ^^ (x => UnaryOp("|", x)) |
      "(" ~> expr <~ ")" |
      "sqrt(" ~> expr <~ ")" ^^ (x => UnaryOp("sqrt", x)) |
      "cos(" ~> expr <~ ")" ^^ (x => UnaryOp("cos", x)) |
      "sin(" ~> expr <~ ")" ^^ (x => UnaryOp("sin", x)) |
      "tan(" ~> expr <~ ")" ^^ (x => UnaryOp("tan", x)) |
      "lg(" ~> expr <~ ")" ^^ (x => UnaryOp("lg", x)) |
      "ln(" ~> expr <~ ")" ^^ (x => UnaryOp("ln", x)) |
      ident ^^ {
        case "Pi" => Number(BigDecimal(MathUtil.PI))
        case "e"  => Number(BigDecimal(MathUtil.E))
      } |
      floatingPointNumber ^^ { x =>
        Number(BigDecimal(x))
      }

  def parse(string: String): ParseResult[Expression] = parseAll(expr, string)
}

object MathUtil {
  val PI: Double = Math.PI
  val E: Double  = Math.E

}

object Calculator {

  def evaluate(string: String): BigDecimal = {

    def aux(expr: Expression): BigDecimal =
      expr match {
        case Number(x)               => x
        case UnaryOp("-", x)         => -aux(x)
        case UnaryOp("+", x)         => aux(x)
        case UnaryOp("|", x)         => aux(x).abs
        case UnaryOp("sqrt", x)      => math.sqrt(aux(x).toDouble)
        case UnaryOp("cos", x)       => math.cos(aux(x).toDouble)
        case UnaryOp("sin", x)       => math.sin(aux(x).toDouble)
        case UnaryOp("tan", x)       => math.tan(aux(x).toDouble)
        case UnaryOp("lg", x)        => math.log10(aux(x).toDouble)
        case UnaryOp("ln", x)        => math.log(aux(x).toDouble)
        case BinaryOp("mod", x1, x2) => aux(x1) % aux(x2)
        case BinaryOp("+", x1, x2)   => aux(x1) + aux(x2)
        case BinaryOp("-", x1, x2)   => aux(x1) - aux(x2)
        case BinaryOp("*", x1, x2)   => aux(x1) * aux(x2)
        case BinaryOp("/", x1, x2)   => aux(x1) / aux(x2)
        case BinaryOp("^", x1, x2)   => aux(x1).pow(aux(x2).toInt)
        case _                       => 0.0
      }

    aux(ExpressionParser.parse(string).get)
  }
}
