package service

import cats.implicits._
import cats.effect._
import expression.Calculator
import io.circe._
import model.SiUnitModel
import org.http4s._
import org.http4s.circe._
import org.http4s.dsl._
import org.http4s.dsl.impl._
import scala.util.control.Breaks._

import scala.math.BigDecimal

final class UnitConversionService[F[_]] extends Http4sDsl[F] {

  private def convertUnit(input: String): Json = {
    val unitsToConvert: Array[String] = input.split(Array('/', '(', ')', '*', '+', '-')).filterNot(x => x == "")
    var convertedSIString             = new StringBuilder(input)
    var convertedMFString             = new StringBuilder(input)

    for (unitToConvert <- unitsToConvert) {
      if (convertedSIString.toString() === "Bad Input") break
      val siUnitAndFactorTuple = SiUnitModel.lookup(unitToConvert)

      siUnitAndFactorTuple match {
        case Some(_) =>
          convertedSIString = new StringBuilder(
            convertedSIString.replaceAllLiterally(unitToConvert, siUnitAndFactorTuple.get._1)
          )
          convertedMFString = new StringBuilder(
            convertedMFString.replaceAllLiterally(unitToConvert, siUnitAndFactorTuple.get._2.toString())
          )
        case None =>
          convertedSIString = new StringBuilder("Bad Input")
          convertedMFString = new StringBuilder("0.0")
      }
    }

    val fieldList =
      List(
        ("unit_name", Json.fromString(convertedSIString.toString())),
        ("multiplication_factor", Json.fromBigDecimal(evaluateMultiplicationFactor(convertedMFString.toString())))
      )

    Json.fromFields(fieldList)
  }

  private def evaluateMultiplicationFactor(input: String): BigDecimal = {
    implicit def bigDecimalToJavaBigDecimal(b: math.BigDecimal) = b.underlying

    Calculator.evaluate(input).setScale(14, BigDecimal.RoundingMode.HALF_DOWN).stripTrailingZeros()
  }

  private def isBalanced(chars: List[Char]): Boolean = {
    def balanced(chars: List[Char], count: Int): Boolean =
      if (chars.isEmpty) count == 0
      else if (count < 0) false
      else if (chars.head == '(') balanced(chars.tail, count + 1)
      else if (chars.head == ')') balanced(chars.tail, count - 1)
      else balanced(chars.tail, count)

    balanced(chars, 0)
  }

  def service()(implicit F: ConcurrentEffect[F]): HttpRoutes[F] =
    HttpRoutes
      .of[F] {
        case GET -> Root / "units" / "si" :? rawUnitParam(rawUnit) =>
          if (!rawUnit.isEmpty && isBalanced(rawUnit.toList)) {
            Ok(convertUnit(rawUnit))
          } else BadRequest()
      }
}

object rawUnitParam extends QueryParamDecoderMatcher[String]("units")
