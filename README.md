# Unit Conversion Service  Api

## Compile Service

sbt compile

## Run Service

sbt run

## Tech-Stack

- [Http4s](http://http4s.org/) the web server
- [Circe](https://circe.github.io/circe/) for json serialization
- [Cats](https://typelevel.org/cats/) for pure FP
- [ScalaTest](https://www.scalatest.org/) for testing
- [PureConfig](https://pureconfig.github.io/docs/) for app config
- [ScalaParserCombinator](https://github.com/scala/scala-parser-combinators) for expression parsing
- Tagless Final for the core domain.

## Usage
- Service runs on http://localhost/units/si 
- Issue GET against REST API using samples like
```
curl "localhost/units/si?unit=degree/minute -X GET"
curl "localhost/units/si?unit=degree -X GET"
curl "localhost/units/si?unit=ha*' -X GET"
```
- In case bad input (unsupported unit) a "Bad Input" will be displayed
- In case an imbalanced expression is passed into the service it will respond with a BadRequest
- In case a bad URL is entered it will respond with a NotFound

 


